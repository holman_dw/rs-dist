all: clean cargo run

run:
	python src/python/run.py

build: cargo

cargo:
	cargo build --release
clean:
	cargo clean
