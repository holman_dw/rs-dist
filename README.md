# Some geometry/geodesy stuff in rust

This is a toy project for doing some CPU intensive calculations in `Rust`;
these will be all exposed through FFI. Vincenty and Great Circle
distances are available for Points and Lines. At this time, only points
are exposed to FFI. I need to determine the best way to pass vectors or
slices of points through FFI.


# Benchmarking
Initial benchmarking has been done on an old laptop with 8GB RAM and a
Intel Core i5-3210M @ 2.5GHz x 2 processor.

The `test_data` python function yields the following results:

| Algorithm | Result (meters) | Time |
| --------- | ------ | ---- |
| Rust Vincenty | 142271879.5045 | 2.9104 ms |
| Rust Line Vincenty | 142271879.5045 | 3.4866 ms |
| Python Vincenty | 142271874.9295 | 42.2353 ms |

Where `Rust Vincenty` is generating the distance along a python list
of points, `Rust Line Vincenty` is generating a `C Array` of points and
passing that to the Rust algorithm, and `Python Vincenty` is doing all of
the work in python.

# Licence
MIT. See `LICENSE` for more details.

# Contact
Reach out on twitter [@holman_dw](https://twitter.com/holman_dw)

