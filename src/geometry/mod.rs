
use std::iter::Iterator;
use std::f64;


#[repr(C)]
#[derive(Debug, PartialEq, Clone)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

#[repr(C)]
#[derive(Debug, PartialEq, Clone)]
pub struct Line {
    pub points: Vec<Point>,
}

#[repr(C)]
#[derive(Debug, PartialEq, Clone)]
pub struct MultiLine {
    pub lines: Vec<Line>,
}

impl Point {
    pub fn new(x: f64, y: f64) -> Self {
        Point { x, y }
    }

    pub fn great_circle(&self, other: &Point) -> Option<f64> {
        let (lat1, lat2) = (self.y.to_radians(), other.y.to_radians());
        let (lng1, lng2) = (self.x.to_radians(), other.x.to_radians());
        let (slat1, clat1) = (lat1.sin(), lat1.cos());
        let (slat2, clat2) = (lat2.sin(), lat2.cos());
        let lng_delta = lng2 - lng1;
        let (cdl, sdl) = (lng_delta.cos(), lng_delta.sin());
        let d = (clat2 * sdl).powi(2) +
            (clat1 * slat2 - slat1 * clat2 * cdl).powi(2).atan2(
                slat1 * slat2 +
                    clat1 * clat2 *
                        cdl,
            );
        Some(d.to_degrees() * 6371.009)
    }

    pub fn vincenty(&self, other: &Self) -> Option<f64> {
        // semi-major, semi-minor, flatting
        let (a, b, f) = (6378137_f64, 6356752.314245_f64, 298.257223563_f64.recip());
        let one = 1f64; // annoying to type 0f64 a million times
        let u1 = ((one - f) * self.x.to_radians().tan()).atan();
        let u2 = ((one - f) * other.x.to_radians().tan()).atan();
        let delta = (self.y - other.y).to_radians();
        let sin_u1 = u1.sin();
        let cos_u1 = u1.cos();
        let sin_u2 = u2.sin();
        let cos_u2 = u2.cos();
        let max_iter = 200;
        // mutable values to be iteratively refined
        let mut lambda = delta;
        let mut cos_sq_alpha = 0f64;
        let mut sin_sigma = 0f64;
        let mut cos_2_sigma_m = 0f64;
        let mut cos_sigma = 0f64;
        let mut sigma = 0f64;
        // refine the above mutable values.
        for i in 0..max_iter {
            let sin_lambda = lambda.sin();
            let cos_lambda = lambda.cos();
            sin_sigma = ((cos_u2 * sin_lambda).powi(2) +
                             (cos_u1 * sin_u2 - sin_u1 * cos_u2 * cos_lambda).powi(2))
                .sqrt();
            // coincident points, 0 distance.
            if sin_sigma == 0f64 {
                return Some(0f64);
            }
            cos_sigma = sin_u1 * sin_u2 + cos_u1 * cos_u2 * cos_lambda;
            sigma = sin_sigma.atan2(cos_sigma);
            let sin_alpha = cos_u1 * cos_u2 * sin_lambda / sin_sigma;
            cos_sq_alpha = (one - sin_alpha).powi(2);
            if cos_sq_alpha != 0f64 {
                cos_2_sigma_m = cos_sigma - 2f64 * sin_u1 * sin_u2 / cos_sq_alpha;
            } else {
                cos_2_sigma_m = 0f64;
            }
            let c = f / 16f64 * cos_sq_alpha * (4f64 + f * (4f64 - 3f64 * cos_sq_alpha));
            let lambda_prev = lambda;
            lambda = delta +
                (one - c) * f * sin_alpha *
                    (sigma +
                         c * sin_sigma *
                             (cos_2_sigma_m +
                                 c * cos_sigma * (-one + 2f64 * cos_2_sigma_m.powi(2))));
            // if we have a successful convergence, break early
            if (lambda - lambda_prev).abs() < 10e-12 {
                break;
            }
            // if it gets to this point and iterated 199 times
            if i == max_iter - 1 {
                return None;
            }
        }
        let u_sq = cos_sq_alpha * (a.powi(2) - b.powi(2)) / (b.powi(2));
        let aa = one +
            u_sq / 16384_f64 * (4096_f64 + u_sq * (-768_f64 + u_sq * (320_f64 - 175_f64 * u_sq)));
        let bb = u_sq / 1024_f64 * (256_f64 + u_sq * (-128_f64 + u_sq * (74_f64 - 47_f64 * u_sq)));
        let delta_sigma = bb * sin_sigma *
            (cos_2_sigma_m +
                 bb / 4_f64 *
                     (cos_sigma * (-1_f64 + 2_f64 * cos_2_sigma_m.powi(2)) -
                          bb / 6_f64 * cos_2_sigma_m * (-3_f64 + 4_f64 * sin_sigma.powi(2)) *
                              (-3_f64 + 4_f64 * cos_2_sigma_m.powi(2))));
        let s = (b * aa * (sigma - delta_sigma)) / 1000f64;
        Some(s)
    }
}

impl Line {
    pub fn new(points: Vec<Point>) -> Self {
        Line { points }
    }

    pub fn great_circle(&self) -> Option<f64> {
        let len = self.points.len();
        let mut acc = 0f64;
        for i in 0..len {
            if i + 1 == len {
                break;
            }
            let p1 = &self.points[i];
            let p2 = &self.points[i + 1];
            let dist = match p1.great_circle(p2) {
                Some(d) => d,
                None => 0f64,
            };
            acc += dist;
        }
        Some(acc)
    }

    pub fn vincenty(&self) -> Option<f64> {
        let len = self.points.len();
        let mut acc = 0f64;
        for i in 0..len {
            if i + 1 == len {
                break;
            }
            let p1 = &self.points[i];
            let p2 = &self.points[i + 1];
            let dist = match p1.vincenty(p2) {
                Some(d) => d,
                None => 0f64,
            };
            acc += dist;
        }
        Some(acc)
    }
    // simplify a line with the Douglas Peucker algorithm
    // and return a copy of the new geometry.
    pub fn douglas_peucker(&self, epsilon: f64) -> Self {
        let mut pts = self.points.clone();
        let smoothed = douglas_peucker(&pts, epsilon);
        Line { points: smoothed }
    }
}

fn douglas_peucker(points: &[Point], epsilon: f64) -> Vec<Point> {
    let mut dmax = 0_f64;
    let mut index = 0;
    let pts = points.clone();
    let len = points.len();
    for i in 1..len {
        let segment = (pts[i].clone(), pts[len - 1].clone());
        // TODO
        let d = perpendicular_distance(pts[i].clone(), segment);
        if d > dmax {
            index = i;
            dmax = d;
        }
    }
    if dmax > epsilon {
        let r1 = douglas_peucker(&pts[0..index], epsilon);
        let r2 = douglas_peucker(&pts[index..], epsilon);
        let mut res = Vec::with_capacity(r1.len() + r2.len());
        for item in r1.iter() {
            res.push(item.clone());
        }
        for item in r2.iter() {
            res.push(item.clone());
        }

        return res;
    } else {
        return vec![pts[0].clone(), pts[len - 1].clone()];
    }
}

fn perpendicular_distance(pt: Point, segment: (Point, Point)) -> f64 {
    let (p1, p2) = segment;
    let a = pt.x - p1.x;
    let b = pt.y - p1.y;
    let c = p2.x - p1.x;
    let d = p2.y - p1.y;
    let e = d * -1_f64;
    let f = c;
    let dot = a * e + b * f;
    let len_sq = e * e + f * f;
    dot.abs() / len_sq.sqrt()
}

impl MultiLine {
    pub fn new(lines: Vec<Line>) -> Self {
        MultiLine { lines }
    }

    pub fn great_circle(&self) -> Option<f64> {
        let dist = self.lines.iter().fold(0f64, |acc, d| {
            acc +
                match d.great_circle() {
                    Some(dd) => dd,
                    None => 0f64,
                }
        });
        Some(dist)
    }

    pub fn vincenty(&self) -> Option<f64> {
        let dist = self.lines.iter().fold(0f64, |acc, d| {
            acc +
                match d.vincenty() {
                    Some(dd) => dd,
                    None => 0f64,
                }
        });
        Some(dist)
    }
}
