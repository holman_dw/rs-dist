"""
demo of using ffi to get the great circle distance
"""

from contextlib import contextmanager
from collections import defaultdict
import json
import os
import time

try:
    from geopy.distance import great_circle as _great_circle, vincenty as _vincenty
except ImportError:
    _great_circle, _vincenty = None, None

from cffi import FFI as _FFI

_ffi = _FFI()
_ffi.cdef("""
    typedef struct {
        double x, y;
    } Point;
    double vincenty(const Point *point, const Point *other);
    double great_circle(const Point *point, const Point *other);
    double vincenty_line(size_t, Point*);
""")

_C = _ffi.dlopen("target/release/libdist.so")

TIMES = defaultdict(float)

class Point:
    """a point"""
    def __init__(self, x, y):
        point = _ffi.new("Point *")
        self._x = x
        self._y = y
        point.x = x
        point.y = y
        self._pt = point

    def __repr__(self):
        return "<{c}({x}, {y})>".format(
                c=self.__class__.__name__,
                x=self._x,
                y=self._y
            )

    @property
    def cpoint(self):
        """pointer to the internal point struct"""
        return self._pt

    def great_circle(self, other):
        """gets the great circle distance in km"""
        return _C.great_circle(self.cpoint, other.cpoint)

    def vincenty(self, other):
        """gets the vincenty distance in km"""
        return _C.vincenty(self.cpoint, other.cpoint)

    @property 
    def tuple(self):
        return self._x, self._y


class Line:
    def __init__(self, *points):
        self._points = points
        self._n = len(points)
        line = _ffi.new("Point[]", self._n)
        for i, pt in enumerate(points):
            line[i].x = pt._pt.x
            line[i].y = pt._pt.y
        self._line = line

    def vincenty(self):
        """generate a vincenty length of an array of c structs"""
        return _C.vincenty_line(self._n, self._line)
    
    def __repr__(self):
        return "<{c}(*{pts})>".format(
                c=self.__class__.__name__,
                pts=self._points
            )

    @classmethod
    def from_raw(cls, *points):
        """generate a Line from any number of (X, Y) pairs"""
        return cls(*[Point(p[0], p[1]) for p in points])


@contextmanager
def timer(key):
    """context manager to time stuff in milliseconds"""
    start = time.time()
    try:
        yield
    finally:
        elapsed = time.time() - start
        TIMES[key] += elapsed * 1000


def parse_json(f):
    with open(f, 'r') as o:
        data = json.load(o)
    points = list()
    for d in data:
        point = Point(*d["coordinates"])
        points.append(point)
    return points
   

def test_data(points):
    acc = 0
    prev = None
    with timer("rust"):
        for point in points:
            if not prev:
                prev = point
                continue
            acc += prev.vincenty(point)
            prev = point
        print("rust     : {}".format(acc * 1000))
    with timer("rust-line"):
        line = Line(*points)
        dist = line.vincenty()
        print("rust-line: {}".format(dist * 1000))
    acc = 0
    with timer("python"):
        prev = None
        for point in points:
            if not prev:
                prev = point.tuple
                continue
            acc += _vincenty(prev, point.tuple).meters
            prev = point.tuple
        print("python   : {}".format(acc))


def main(num_iters=500000):
    """runs some benchmarks. it is intentional that
    the allocations are within each iteration; this is
    to determine if it's worth it to use rust even with
    the allocations."""
    for _ in range(num_iters):
        with timer("rust-gc"):
            pt1 = Point(0, 0)
            pt2 = Point(0, 1)
            out = pt1.great_circle(pt2)
    print("rust-gc    ", out * 1000)
    if great_circle:
        for _ in range(num_iters):
            with timer("python-gc"):
                out = _great_circle((0.0, 0.0), (0.0, 1.0)).meters
        print("python-gc  ", out)
    for _ in range(num_iters):
        with timer("rust-v"):
            pt1 = Point(0, 0)
            pt2 = Point(0, 1)
            out = pt1.vincenty(pt2)
    print("rust-v     ", out * 1000)
    if vincenty:
        for _ in range(num_iters):
            with timer("python-v"):
                out = _vincenty((0.0, 0.0), (0.0, 1.0)).meters
        print("python-v   ", out)


if __name__ == "__main__":
    f = "./data/geoms1000.json"
    if not os.path.exists(f):
        f = "./data/geoms.json"
    data = parse_json(f)
    test_data(data)
    for k, v in TIMES.items():
        print("{}:\n\t{} ms".format(k, v))

