#![allow(unused)]
extern crate libc;
use std;
use std::iter::Iterator;
use std::f64;
use Point;
use Line;


#[no_mangle]
pub extern "C" fn great_circle(pt: *const Point, other: *const Point) -> f64 {
    let p1 = unsafe {
        assert!(!pt.is_null());
        &*pt
    };
    let p2 = unsafe {
        assert!(!other.is_null());
        &*other
    };
    let dist = match p1.great_circle(&p2) {
        Some(d) => d,
        None => 0f64,
    };
    dist
}

#[no_mangle]
pub extern "C" fn vincenty(pt: *const Point, other: *const Point) -> f64 {
    let p1 = unsafe {
        assert!(!pt.is_null());
        &*pt
    };
    let p2 = unsafe {
        assert!(!other.is_null());
        &*other
    };
    let dist = match p1.vincenty(&p2) {
        Some(d) => d,
        None => 0f64,
    };
    dist
}

#[no_mangle]
pub extern "C" fn new_line(pts: *const Point, pt_len: usize) -> Line {
    let points = unsafe { std::slice::from_raw_parts(pts, pt_len) };
    let pts = points.to_vec();
    Line { points: pts }
}

#[no_mangle]
pub extern "C" fn vincenty_line(size: libc::size_t, array_pointer: *const libc::uint32_t) -> f64 {
    let points = unsafe {
        assert!(!array_pointer.is_null());
        std::slice::from_raw_parts(array_pointer as *const Point, size as usize)
    };
    let line = Line { points: points.into() };
    let dist = match line.vincenty() {
        Some(d) => d,
        None => 0f64,
    };
    dist
}
